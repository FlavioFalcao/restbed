/*
 * Copyright (c) 2013, 2014 Corvusoft
 */

#ifndef _RESTBED_BYTE_H
#define _RESTBED_BYTE_H 1

//System Includes
#include <cstdint>

//Project Includes

//External Includes

//System Namespaces

//Project Namespaces

//External Namespaces

namespace restbed
{
    typedef uint8_t Byte;
}

#endif  /* _RESTBED_BYTE_H */
