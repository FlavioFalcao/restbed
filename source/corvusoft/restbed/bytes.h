/*
 * Copyright (c) 2013, 2014 Corvusoft
 */

#ifndef _RESTBED_BYTES_H
#define _RESTBED_BYTES_H 1

//System Includes
#include <vector>

//Project Includes
#include <corvusoft/restbed/byte>

//External Includes

//System Namespaces

//Project Namespaces

//External Namespaces

namespace restbed
{
    typedef std::vector< Byte > Bytes;
}

#endif  /* _RESTBED_BYTES_H */
