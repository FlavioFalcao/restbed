# Copyright (c) 2013, 2014 Corvusoft

#
# Build manifest
#
set( MANIFEST
	${SOURCE_DIR}/test_logger.cpp
	${SOURCE_DIR}/test_service.cpp
	${SOURCE_DIR}/service_helper.cpp
	${SOURCE_DIR}/basic_auth_service.cpp
)
